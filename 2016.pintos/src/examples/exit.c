/* Tests the exit system call. */

#include "tests/lib.h"

void
main (void)
{
  exit (57);
  fail ("should have called exit(57)");
}

/* Wait for a subprocess to finish. */

#include <syscall.h>
#include "tests/lib.h"
#include "tests/main.h"

void
main (void)
{
  printf ("wait(exec()) = %d", wait (exec ("child-simple")));
}
